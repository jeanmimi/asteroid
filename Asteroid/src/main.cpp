#include "Game.hpp"
#include "Configuration.hpp"

int main(int argc, char* argv[])
{
  Configuration::initialize();
  
  Game game;
  game.run(30); //fixed time steps
  
  return 0;
}
