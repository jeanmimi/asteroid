#ifndef _ACTION_HPP
#define _ACTION_HPP

#include <SFML/Window/Event.hpp>

class Action
{
public:
  enum Type
  {
    RealTime = 1,
    Pressed  = 1<<1,
    Released = 1<<2
  };

  /**
   * constructor with keyboard code
   *   @param key
   *      The keyboard code that will trigger the action
   *   @param type
   *      The type of event
   */
  Action(const sf::Keyboard::Key& key, int type=Type::RealTime|Type::Pressed);

  /**
   * constructor with mouse button
   *   @param button
   *      The mouse button that will trigger the event
   *   @param type
   *      The type of event
   */
  Action(const sf::Mouse::Button& button, int type=Type::RealTime|Type::Pressed);

  /**
   * copy constructor and copy operator
   */
  Action(const Action& other);
  Action& operator=(const Action& other);
  
  bool operator==(const Action& action) const;
  bool operator==(const sf::Event& event) const;
  
  /**
   * check if the event has to be triggered
   *   @return
   *      true if event have to trigger, else false
   */
  bool checkTrigger() const;

  const sf::Event& getEvent() const {return _event;};
  const int getType() const {return _type;};
private:

  bool isKeyPressed() const;
  bool isKeyReleased() const;
  bool isMousePressed() const;
  bool isMouseReleased() const;
  
  sf::Event _event;
  int _type;
};
  
#endif
