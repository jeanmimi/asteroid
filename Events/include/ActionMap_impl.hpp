
template<typename T>
void ActionMap<T>::set(const T& key, const Action& action)
{
  _hmap.emplace(key, action);
}

template<typename T>
const Action& ActionMap<T>::get(const T& key) const
{
  return _hmap.at(key);
}
