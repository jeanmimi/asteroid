#ifndef _ACTIONTARGET_HPP
#define _ACTIONTARGET_HPP

#include <functional>
#include <list>
#include <utility>
#include <SFML/Window.hpp>
#include "ActionMap.hpp"


template<typename T = int>
class ActionTarget
{
public:
  ActionTarget(const ActionMap<T>& actionMap);
  virtual ~ActionTarget() = default;
  ActionTarget(const ActionTarget<T>&) = delete;
  ActionTarget<T>& operator=(const ActionTarget<T>&) = delete;

  using FuncType = std::function<void(const sf::Event&)>;
  
  bool processEvent(const sf::Event& event) const;
  void processEvents() const;

  void bind(const T& key, const FuncType& callback);
  void unbind(const T& key);

private:
  std::list<std::pair<T, FuncType> > _eventsRealTime;
  std::list<std::pair<T, FuncType> > _eventsPoll;
  const ActionMap<T>& _actionMap;
};

#include "ActionTarget_impl.hpp"
#endif
