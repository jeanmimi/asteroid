template<typename T>
ActionTarget<T>::ActionTarget(const ActionMap<T>& actionMap):
  _actionMap(actionMap)
{
}

template<typename T>
bool ActionTarget<T>::processEvent(const sf::Event& event) const
{
  bool res = false;

  //look for event in the list _eventsPoll to trigger its callback
  //TODO: use stl algorithm
  for (auto& pair: _eventsPoll) {
    const Action& action = _actionMap.get(pair.first);
    if (action == event) {
      //trigger the callback
      pair.second(event);
      res = true;
      break;
    }
  }

  return res;
}

template<typename T>
void ActionTarget<T>::processEvents() const
{
  //for all actions in realtime, trigger callback if event is checkTrigger
  //TODO: use STL algorithm 
  for (auto& pair: _eventsRealTime) {
    const Action& action = _actionMap.get(pair.first);
    if (action.checkTrigger()) {
      pair.second(action.getEvent());
    }
  }
}

template<typename T>
void ActionTarget<T>::bind(const T& key, const FuncType& callback)
{
  const Action& action = _actionMap.get(key);
  if (action.getType() & Action::Type::RealTime) {
    _eventsRealTime.emplace_back(key, callback);
  } else {
    _eventsPoll.emplace_back(key, callback);
  }
}

template<typename T>
void ActionTarget<T>::unbind(const T& key)
{
  auto remove_func = [&key](const std::pair<T, FuncType>& pair) -> bool
                     {return pair.first == key;};

  const Action& action = _actionMap.get(key);
  if (action.getType() & Action::Type::RealTime) {
    _eventsRealTime.remove_if(remove_func);
  } else {
    _eventsPoll.remove_if(remove_func);
  }
}
