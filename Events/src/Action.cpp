#include "Action.hpp"
#include <cstring>

//-----------------------------------------------------------------------
Action::Action(const sf::Keyboard::Key& key, int type):
  _type(type)
{
  //type could be a combination of Pressed and Released
  //but this EventType does not exist in sfml
  //so init just with KeyPressed and we deal with the real type
  //during checkTrigger
  _event.type = sf::Event::EventType::KeyPressed;
  _event.key.code = key;
}

//-----------------------------------------------------------------------
Action::Action(const sf::Mouse::Button& button, int type):
  _type(type)
{
  //type could be a combination of Pressed and Released
  //but this EventType does not exist in sfml
  //so init just with MouseButtonPressed and we deal with the real type
  //during checkTrigger
  _event.type = sf::Event::EventType::MouseButtonPressed;
  _event.mouseButton.button = button;
}

//-----------------------------------------------------------------------
Action::Action(const Action& other):
  _type(other._type)
{
  //sf::Event is non-copyable, so need to use memcpy
  std::memcpy(&_event,&other._event, sizeof(sf::Event));
}

//-----------------------------------------------------------------------
Action& Action::operator=(const Action& action)
{
  _type = action._type;
  //sf::Event is non-copyable, so need to use memcpy
  std::memcpy(&_event,&action._event, sizeof(sf::Event));
  return *this;
}

bool Action::isKeyPressed() const
{
  return (_type & Type::Pressed) != 0 && _event.type == sf::Event::EventType::KeyPressed;
}
bool Action::isKeyReleased() const
{
  return (_type & Type::Released) != 0 && _event.type == sf::Event::EventType::KeyPressed;
}
bool Action::isMousePressed() const
{
  return (_type & Type::Pressed) != 0 && _event.type == sf::Event::EventType::MouseButtonPressed;
}
bool Action::isMouseReleased() const
{
  return (_type & Type::Released) != 0 && _event.type == sf::Event::EventType::MouseButtonPressed;
}

//-----------------------------------------------------------------------
bool Action::operator==(const sf::Event& event) const
{
  bool res = false;
  
  switch (event.type) {
    case sf::Event::EventType::KeyPressed: {
      if (isKeyPressed()) {
        res = event.key.code == _event.key.code;
      }
    } break;
    case sf::Event::EventType::KeyReleased: {
      if (isKeyReleased()) {
        res = event.key.code == _event.key.code;
      }
    } break;
    case sf::Event::EventType::MouseButtonPressed: {
      if (isMousePressed()) {
        res = event.mouseButton.button == _event.mouseButton.button;
      }
    } break;
    case sf::Event::EventType::MouseButtonReleased: {
      if (isMouseReleased()) {
        res = event.mouseButton.button == _event.mouseButton.button;
      }
    } break;
  default: break;
  }

  return res;
}

//-----------------------------------------------------------------------
bool Action::operator==(const Action& action) const
{
  //test action == _event will call our redefined operator==(const Event&)
  return _type == action._type && action == _event;
}

//-----------------------------------------------------------------------
bool Action::checkTrigger() const
{
  bool res = false;
  if (isKeyPressed()) {
    res = sf::Keyboard::isKeyPressed(_event.key.code);
  } else if (isMousePressed()) {
    res = sf::Mouse::isButtonPressed(_event.mouseButton.button);
  }

  return res;
}
