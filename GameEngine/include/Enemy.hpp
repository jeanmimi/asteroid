#ifndef _ENEMY_HPP
#define _ENEMY_HPP

#include "Entity.hpp"
#include "Configuration.hpp"

class World;

class Enemy: public Entity
{
public:
  Enemy(Configuration::Textures id, World& world, Score& score);
  Enemy(const Enemy&) = delete;
  Enemy& operator=(const Enemy&) = delete;

  virtual int getPoints() const = 0;
  virtual void onDestroy();
};

#endif
