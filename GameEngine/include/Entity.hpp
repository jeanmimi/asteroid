#ifndef _ENTITY_HPP
#define _ENTITY_HPP

#include <SFML/Graphics.hpp>
#include "Configuration.hpp"

class World;
class Score;

class Entity: public sf::Drawable
{
public:
  Entity(Configuration::Textures id, World& world, Score& score);
  virtual ~Entity() = default;
  Entity(const Entity&) = delete;
  Entity& operator=(const Entity&) = delete;

  virtual bool isAlive() const;

  const sf::Vector2f& getPosition() const;
  template<typename ... Args>
  void setPosition(Args&& ... args);
  virtual bool isCollide(const Entity& other) const = 0;

  virtual void update(sf::Time deltaTime) = 0;
  virtual void onDestroy();

  const sf::Sprite& getSprite() const {return _sprite;};
  World& getWorld() {return _world;};
  Score& getScore() {return _score;};
  
protected:
  sf::Sprite _sprite;
  sf::Vector2f _impulse;
  World& _world;
  Score& _score;
  bool _alive = true;
  
private:
  virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const override;

};

#include "Entity_impl.hpp"
#endif
