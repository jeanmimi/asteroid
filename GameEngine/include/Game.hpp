#ifndef _GAME_HPP
#define _GAME_HPP

#include <SFML/Graphics.hpp>
#include "World.hpp"
#include "Score.hpp"

class Player;

class Game
{
public:
  Game(int x=1600, int y=900);
  ~Game() = default;
  Game(const Game&) = delete;
  Game& operator=(const Game&) = delete;
  void run(int minimu_frame_per_second);
  void initLevel();
  void initPlayer();

private:
  void processEvents();
  void update(sf::Time deltaTime);
  void render();
  void reset();
  
  sf::RenderWindow _window;
  World _world;
  Player* _player = nullptr;
  sf::Time   _nextSaucer;
  sf::Text   _txt;
  Score _score;
};

#endif
