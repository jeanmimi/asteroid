#ifndef _METEOR_HPP
#define _METEOR_HPP

#include "Enemy.hpp"

class Entity;
class SCore;

class Meteor: public Enemy
{

public:
  Meteor(const Meteor&) = delete;
  Meteor& operator=(const Meteor&) = delete;
  Meteor(Configuration::Textures id, World& world, Score& score);
  
  using Enemy::Enemy;
  
  virtual bool isCollide(const Entity& other) const;
  virtual void update(sf::Time deltaTime);
};

class BigMeteor : public Meteor
{
public :
  BigMeteor(World& world, Score& score);
  virtual int getPoints() const override;
  virtual void onDestroy() override;
};

class MediumMeteor : public Meteor
{
public :
  MediumMeteor(World& world, Score& score);
  virtual int getPoints()const override;
  virtual void onDestroy() override;
};

class SmallMeteor : public Meteor
{
public :
  SmallMeteor(World& world, Score& score);
  virtual int getPoints() const override;
  virtual void onDestroy() override;
};
#endif
