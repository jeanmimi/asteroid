#ifndef _PLAYER_HPP
#define _PLAYER_HPP

#include <utility>
#include <SFML/Graphics.hpp>
#include <SFML/System/Time.hpp>
#include "Entity.hpp"
#include "World.hpp"
#include "ActionTarget.hpp"

class Player: public Entity, public ActionTarget<int>  {
public:
  Player(World& world, Score& score);
  ~Player() = default;
  Player(const Player&) = delete;
  Player& operator=(const Player&) = delete;

  virtual bool isCollide(const Entity& other) const;
  virtual void update(sf::Time deltaTime);
  void processEvents();
  void shoot();
  void goToHyperspace();
  virtual void onDestroy();

private:
  bool _isMoving = false;
  int _rotation = 0;
  sf::Time _timeSinceLastShoot;
};
  
#endif
