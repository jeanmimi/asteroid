#ifndef _SAUCER_HPP
#define _SAUCER_HPP

#include "Enemy.hpp"

class Entity;
class Score;

class Saucer: public Enemy
{
public:
  using Enemy::Enemy;
  Saucer(const Saucer&) = delete;
  Saucer& operator=(const Saucer&) = delete;

  virtual bool isCollide(const Entity& other) const;
  virtual void update(sf::Time deltaTime);
  virtual void onDestroy() override;

  static void newSaucer(World& world, Score& score);
};


class BigSaucer: public Saucer
{
public:
  BigSaucer(World& world, Score& score);
  ~BigSaucer() = default;
  
  virtual int getPoints() const;
};


class SmallSaucer: public Saucer
{
public:
  SmallSaucer(World& world, Score& score);
  ~SmallSaucer() = default;
  
  virtual int getPoints() const;
  virtual void update(sf::Time deltaTime);

private:
  sf::Time _timeSinceLastShoot;
};

#endif
