#ifndef _SHOOT_HPP
#define _SHOOT_HPP

#include "Entity.hpp"
#include <SFML/System.hpp>

class Shoot: public Entity
{
public:
  Shoot(Configuration::Textures id, World& world, Score& score);
  using Entity::Entity;
  Shoot(const Shoot&) = delete;
  Shoot& operator=(const Shoot&) = delete;

  virtual void update(sf::Time deltaTime) override;

protected:
  sf::Time _duration;
};

#endif
