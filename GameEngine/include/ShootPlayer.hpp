#ifndef _SHOOTPLAYER_HPP
#define _SHOOTPLAYER_HPP

#include "Shoot.hpp"

class Player;
class Entity;
class Score;

class ShootPlayer: public Shoot
{
public:
  ShootPlayer(Player& from, Score& score);
  ShootPlayer(const ShootPlayer&) = delete;
  ShootPlayer& operator=(const ShootPlayer&) = delete;

  virtual bool isCollide(const Entity& other) const override;
};

#endif
