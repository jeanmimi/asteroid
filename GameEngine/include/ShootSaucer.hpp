#ifndef _SHOOTSAUCER_HPP
#define _SHOOTSAUCER_HPP

#include "Shoot.hpp"

class SmallSaucer;
class Entity;
class Score;

class ShootSaucer: public Shoot
{
public:
  ShootSaucer(SmallSaucer& from, Score& score);
  ShootSaucer(const ShootSaucer&) = delete;
  ShootSaucer& operator=(const ShootSaucer&) = delete;

  virtual bool isCollide(const Entity& other) const override;
};

#endif
