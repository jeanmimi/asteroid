#ifndef _WORLD_HPP
#define _WORLD_HPP

#include <list>
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include "Configuration.hpp"

class Entity;
class Player;

class World: public sf::Drawable
{
public:
  World(float x, float y);
  ~World();
  World(const World&) = delete;
  World& operator=(const World&) = delete;

  void add(std::unique_ptr<Entity> entity);
  void clear();
  bool isCollide(const Entity& other);
  int size();
  void add(Configuration::Sounds id);
  const std::list<std::unique_ptr<Entity>>& getEntities() const;
  Player* getPlayer();
  int getX() const;
  int getY() const;
  void update(sf::Time deltaTime);

private:
  virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const override;
  void clampPosition(Entity& entity);
  
  std::list<std::unique_ptr<Entity>> _entities;
  std::list<std::unique_ptr<Entity>> _entitiesTmp; 
  std::list<std::unique_ptr<sf::Sound>> _sounds;
  const int _x;
  const int _y;
};

#endif
