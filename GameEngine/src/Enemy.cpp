#include "Enemy.hpp"

#define _USE_MATH_DEFINES
#include <cmath>
#include "World.hpp"
#include <SFML/Graphics.hpp>
#include "Configuration.hpp"
#include "Random.hpp"
#include "Score.hpp"

//-----------------------------------------------------------------------
// Enemy constructor
//-----------------------------------------------------------------------
Enemy::Enemy(Configuration::Textures id, World& world, Score& score):
  Entity(id, world, score)
{
  float angle = Tools::Random<float>(0.f, 2.f*M_PI);
  _impulse = sf::Vector2f(std::cos(angle), std::sin(angle));
}

//-----------------------------------------------------------------------
// onDestroy
//-----------------------------------------------------------------------
void Enemy::onDestroy()
{
  Entity::onDestroy();
  _score.addScore(getPoints());
}
