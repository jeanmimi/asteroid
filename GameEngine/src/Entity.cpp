#include "Entity.hpp"

//-----------------------------------------------------------------------
// Entity constructor
//-----------------------------------------------------------------------
Entity::Entity(Configuration::Textures id, World& world, Score& score):
  _world(world),_score(score)
{
  sf::Texture& texture = Configuration::textureManager.get(id);
  _sprite.setTexture(texture);
  _sprite.setOrigin(texture.getSize().x/2.f, texture.getSize().y/2.f);
}

//-----------------------------------------------------------------------
// getPosition
//-----------------------------------------------------------------------
const sf::Vector2f& Entity::getPosition() const
{
  return _sprite.getPosition();
}

//-----------------------------------------------------------------------
// draw
//-----------------------------------------------------------------------
void Entity::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
  target.draw(_sprite, states);
}

//-----------------------------------------------------------------------
// isAlive
//-----------------------------------------------------------------------
bool Entity::isAlive() const
{
  return _alive;
}

//-----------------------------------------------------------------------
// onDestroy
//-----------------------------------------------------------------------
void Entity::onDestroy()
{
  _alive = false;
}
