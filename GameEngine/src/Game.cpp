#include "Game.hpp"
#include <SFML/System.hpp>
#include "Player.hpp"
#include "Saucer.hpp"
#include "Configuration.hpp"
#include "Meteor.hpp"
#include "Random.hpp"
#include <iostream>

//-----------------------------------------------------------------------
// Game constructor
//-----------------------------------------------------------------------
Game::Game(int x, int y):
  _window(sf::VideoMode(x, y),"Asteroid"),
  _world(x,y)
{
  _txt.setFont(Configuration::fontManager.get(Configuration::Fonts::Gui));
  _txt.setCharacterSize(70);
  _txt.setString("Appuyez sur Entree pour commencer");
  
  sf::FloatRect size = _txt.getGlobalBounds();
  _txt.setOrigin(size.width/2,size.height/2);
  _txt.setPosition(x/2,y/2);
}

//-----------------------------------------------------------------------
// run
//-----------------------------------------------------------------------
void Game::run(int minimum_frame_per_second)
{
  sf::Clock clock;
  sf::Time timeSinceLastUpdate = sf::Time::Zero;
  sf::Time timePerFrame = sf::seconds(1.0f/minimum_frame_per_second);
  // _window.setFramerateLimit(minimum_frame_per_second);
  
  while(_window.isOpen()) {
    processEvents();
    
    //loop to ensure fps and avoid lag
    timeSinceLastUpdate = clock.restart();
    while (timeSinceLastUpdate > timePerFrame) {
      timeSinceLastUpdate -= timePerFrame;
      update(timePerFrame);
    }
    update(timeSinceLastUpdate);
    render();
  }
}

//-----------------------------------------------------------------------
// processEvents
//-----------------------------------------------------------------------
void Game::processEvents()
{
  sf::Event event;
  while(_window.pollEvent(event)) {
    if (event.type == sf::Event::Closed) {
      _window.close();
    } else if (event.type == sf::Event::KeyPressed) {
      if (event.key.code == sf::Keyboard::Escape) {
        _window.close();
      } 
    }

    if(_score.isGameOver()) {
      if (event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::Return) {
	      reset();
      }
    } else {
      if(_world.getPlayer()) {
        _world.getPlayer()->processEvent(event);
      }
    }
  }

  if(!_score.isGameOver() && _world.getPlayer()) {
    _world.getPlayer()->processEvents();
  }
}

//-----------------------------------------------------------------------
// update
//-----------------------------------------------------------------------
void Game::update(sf::Time deltaTime)
{
  if(!_score.isGameOver()) {
    _world.update(deltaTime);
    if (!_world.getPlayer()) {
      initPlayer();
    }
  
    _nextSaucer -= deltaTime;
      
    if(_nextSaucer < sf::Time::Zero) {
      Saucer::newSaucer(_world, _score);
      //between 10 and 58 sec before new Saucer
      //less 2 seconds for each level
      _nextSaucer = sf::seconds(Tools::Random<float>(10.f,60.f - _score.getLevel()*2));
    }
      
    if(_world.size() <= 1) {
      _score.increaseLevel();
      initLevel(); //will create new enemies for the level
    }
  }
}

//-----------------------------------------------------------------------
// render
//-----------------------------------------------------------------------
void Game::render()
{
  _window.clear();
  _window.draw(_score);
  if(_score.isGameOver()) {
    _window.draw(_txt);
  } else {
    _window.draw(_world);
  }
 
  _window.display();
}

//-----------------------------------------------------------------------
// initLevel
//-----------------------------------------------------------------------
void Game::initLevel()
{
  int nb_meteors;
  switch(_score.getLevel()) {
    case 1 : nb_meteors = 4;break;
    case 2 : nb_meteors = 5;break;
    case 3 : nb_meteors = 7;break;
    case 4 : nb_meteors = 9;break;
    default : nb_meteors = 11;break;
  }

  for (int i = 0; i < nb_meteors; ++i) {
    std::unique_ptr<Entity> meteor(new BigMeteor(_world, _score));
    do {
      //make sure enemy do no appear in same place as player
      meteor->setPosition(Tools::Random<float>(0.f,(float)_world.getX()),
                          Tools::Random<float>(0.f,(float)_world.getY()));
    } while(_world.isCollide(*meteor));
    _world.add(std::move(meteor));
  }
}

//-----------------------------------------------------------------------
// initPlayer
//-----------------------------------------------------------------------
void Game::initPlayer()
{
  if (!_score.isGameOver()) {
    std::unique_ptr<Entity> player(new Player(_world, _score));
    player->setPosition(_world.getX()/2,_world.getY()/2);
    _world.add(std::move(player));
  }
}

//-----------------------------------------------------------------------
// reset
//-----------------------------------------------------------------------
void Game::reset()
{
  _nextSaucer = sf::seconds(Tools::Random<float>(5.f,6.f - _score.getLevel()*2));
  _world.clear();
  _score.reset();
  initLevel();
}
