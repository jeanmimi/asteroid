#include "Meteor.hpp"
#include "Collision.hpp"
#include "Random.hpp"
#include "World.hpp"



//-----------------------------------------------------------------------
// Meteor constructor
//-----------------------------------------------------------------------
Meteor::Meteor(Configuration::Textures id, World& world, Score& score):
  Enemy(id, world, score)
{
}

//-----------------------------------------------------------------------
// isCollide
//-----------------------------------------------------------------------
bool Meteor::isCollide(const Entity& other) const
{
  //No collision with other meteor
  if (dynamic_cast<const Meteor*>(&other) == nullptr) {
    return Collision::circleTest(_sprite, other.getSprite());
  }
  return false;
}

//-----------------------------------------------------------------------
// update
//-----------------------------------------------------------------------
void Meteor::update(sf::Time deltaTime)
{
  float seconds = deltaTime.asSeconds();
  _sprite.move(seconds * _impulse);
}

//-----------------------------------------------------------------------
// BigMeteor constructor
//-----------------------------------------------------------------------
BigMeteor::BigMeteor(World& world, Score& score):
  Meteor((Configuration::Textures)int(Tools::Random<int>(Configuration::Textures::BigMeteor1,
                                                         Configuration::Textures::BigMeteor4)), world, score)
{
  _impulse *= 100.f;
}

//-----------------------------------------------------------------------
// getPoints
//-----------------------------------------------------------------------
int BigMeteor::getPoints() const
{
  return 20;
}

//-----------------------------------------------------------------------
// onDestroy
//-----------------------------------------------------------------------
void BigMeteor::onDestroy()
{
  Meteor::onDestroy();
  int nb = Tools::Random<int>(2,3);
  for (int i=0; i<nb; ++i) {
    std::unique_ptr<Entity> meteor( new MediumMeteor(_world, getScore()));
    meteor->setPosition(getPosition());
    _world.add(std::move(meteor));
  }
  _world.add(Configuration::Sounds::Explosion1);
}

//-----------------------------------------------------------------------
// MediumMeteor consructor
//-----------------------------------------------------------------------
MediumMeteor::MediumMeteor(World& world, Score& score):
  Meteor((Configuration::Textures)int(Tools::Random<int>(Configuration::Textures::MediumMeteor1,
                                                         Configuration::Textures::MediumMeteor2)), world, score)
{
  _impulse *= 200.f;
}

//-----------------------------------------------------------------------
// getPoints
//-----------------------------------------------------------------------
int MediumMeteor::getPoints() const
{
  return 60;
}

//-----------------------------------------------------------------------
// onDestroy
//-----------------------------------------------------------------------
void MediumMeteor::onDestroy()
{
  Meteor::onDestroy();
  int nb = Tools::Random<int>(2,3);
  for (int i=0; i<nb; ++i) {
    std::unique_ptr<Entity> meteor(new SmallMeteor(_world, getScore()));
    meteor->setPosition(getPosition());
    _world.add(std::move(meteor));
  }
  _world.add(Configuration::Sounds::Explosion2);
}

//-----------------------------------------------------------------------
// SmallMeteor constructor
//-----------------------------------------------------------------------
SmallMeteor::SmallMeteor(World& world, Score& score):
  Meteor((Configuration::Textures)int(Tools::Random<int>(Configuration::Textures::SmallMeteor1,
                                                         Configuration::Textures::SmallMeteor4)), world, score)
{
  _impulse *= 300.f;
}

//-----------------------------------------------------------------------
// getPoints
//-----------------------------------------------------------------------
int SmallMeteor::getPoints() const
{
  return 100;
}

//-----------------------------------------------------------------------
// onDestroy
//-----------------------------------------------------------------------
void SmallMeteor::onDestroy()
{
  Meteor::onDestroy();
  _world.add(Configuration::Sounds::Explosion3);
}
