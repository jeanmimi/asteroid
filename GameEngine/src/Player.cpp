#include "Player.hpp"
#define _USE_MATH_DEFINES
#include <cmath>
#include "ActionTarget.hpp"
#include "Configuration.hpp"
#include "World.hpp"
#include "Random.hpp"
#include "ShootPlayer.hpp"
#include "Collision.hpp"
#include "Score.hpp"

//-----------------------------------------------------------------------
// Player constructor
//-----------------------------------------------------------------------
Player::Player(World& world, Score& score):
  Entity(Configuration::Textures::Player, world, score),
  ActionTarget(Configuration::playerInputs)
{
  bind(Configuration::PlayerInputs::Up,[this](const sf::Event&) {_isMoving = true;});
  bind(Configuration::PlayerInputs::Left,[this](const sf::Event&) {_rotation -= 1;});
  bind(Configuration::PlayerInputs::Right,[this](const sf::Event&) {_rotation += 1;});
  bind(Configuration::PlayerInputs::Shoot,[this](const sf::Event&) {shoot();});
  bind(Configuration::PlayerInputs::Hyperspace,[this](const sf::Event&) {goToHyperspace();});
}

//-----------------------------------------------------------------------
// isCollide
//-----------------------------------------------------------------------
bool Player::isCollide(const Entity& other) const
{
  if (dynamic_cast<const ShootPlayer*>(&other) == nullptr) {
    return Collision::circleTest(_sprite, other.getSprite());
  }
  return false;
}

//-----------------------------------------------------------------------
// shoot
//-----------------------------------------------------------------------
void Player::shoot()
{
  if (_timeSinceLastShoot > sf::seconds(0.3)) {
    _world.add(std::unique_ptr<Entity>(new ShootPlayer(*this, getScore())));
    _timeSinceLastShoot = sf::Time::Zero;
  }
}

//-----------------------------------------------------------------------
// goToHyperspace
//-----------------------------------------------------------------------
void Player::goToHyperspace()
{
  _impulse = sf::Vector2f(0,0);
  setPosition(Tools::Random<int>(0,_world.getX()),
              Tools::Random<int>(0,_world.getY()) );
  _world.add(Configuration::Sounds::Jump);
}

//-----------------------------------------------------------------------
// update
//-----------------------------------------------------------------------
void Player::update(sf::Time deltaTime)
{
  float seconds = deltaTime.asSeconds();
  _timeSinceLastShoot += deltaTime;
  if (_rotation != 0) {
    float angle = _rotation*250*seconds;
    _sprite.rotate(angle);
  }
  
  if (_isMoving) {
    float angle = _sprite.getRotation() / 180 * M_PI - M_PI/2;
    _impulse += sf::Vector2f(std::cos(angle),std::sin(angle))*300.0f*seconds;
  }
  _sprite.move(seconds * _impulse);

}

//-----------------------------------------------------------------------
// onDestroy
//-----------------------------------------------------------------------
void Player::onDestroy()
{
  Entity::onDestroy();
  _score.loseLife();
  _world.add(Configuration::Sounds::Boom);
}

//-----------------------------------------------------------------------
// processEvents
//-----------------------------------------------------------------------
void Player::processEvents()
{
  
  _isMoving = false;
  _rotation = 0;
  ActionTarget::processEvents();
}
