#include "Saucer.hpp"
#include "Random.hpp"
#include "Collision.hpp"
#include "ShootSaucer.hpp"
#include "World.hpp"
#include "Meteor.hpp"
#include "ShootPlayer.hpp"
#include "Player.hpp"
#include "Score.hpp"

//-----------------------------------------------------------------------
// isCollide
//-----------------------------------------------------------------------
bool Saucer::isCollide(const Entity& other) const
{
  if (dynamic_cast<const ShootSaucer*>(&other) == nullptr) {
    return Collision::circleTest(_sprite, other.getSprite());
  }
  return false;
}

//-----------------------------------------------------------------------
// update
//-----------------------------------------------------------------------
void Saucer::update(sf::Time deltaTime)
{
  float seconds = deltaTime.asSeconds();
  Entity* near = nullptr;
  float near_distance = 300;
  for (auto& pEntity:_world.getEntities()) {
    Entity* entity = pEntity.get();
    if (entity != this && (dynamic_cast<const Meteor*>(entity) || dynamic_cast<const ShootPlayer*>(entity))) {
      float x = getPosition().x - entity->getPosition().x;
      float y = getPosition().y - entity->getPosition().y;
      float dist = std::sqrt(x*x + y*y);
      if (dist < near_distance) {
        near_distance = dist;
        near = entity;
      }
    }
  }

  if (near) {
    sf::Vector2f pos = near->getPosition() - getPosition();
    float angle_rad = std::atan2(pos.y, pos.x);
    _impulse -= sf::Vector2f(std::cos(angle_rad), std::sin(angle_rad)) * 300.f * seconds;
  } else {
    sf::Vector2f pos = _world.getPlayer()->getPosition() - getPosition();
    float angle_rad = std::atan2(pos.y, pos.x);
    _impulse -= sf::Vector2f(std::cos(angle_rad), std::sin(angle_rad)) * 100.f * seconds;
  }
  _sprite.move(seconds * _impulse);
}

//-----------------------------------------------------------------------
// onDestroy
//-----------------------------------------------------------------------
void Saucer::onDestroy()
{
  Enemy::onDestroy();
  _world.add(Configuration::Sounds::Boom2);
}

//-----------------------------------------------------------------------
// newSaucer
//-----------------------------------------------------------------------
void Saucer::newSaucer(World& world, Score& score)
{
  std::unique_ptr<Entity> res = nullptr;
  if (Tools::Random<float>(0.f,1.f) > score.getScore()/40000.f) {
    res.reset(new BigSaucer(world, score));
  } else {
    res.reset(new SmallSaucer(world, score));
  }
  res->setPosition(Tools::Random<int>(0,1) * world.getX(),
                   Tools::Random<float>(0.f,(float)world.getY()) );
  world.add(std::move(res));
}

//-----------------------------------------------------------------------
// BigSaucer constructor
//-----------------------------------------------------------------------
BigSaucer::BigSaucer(World& world, Score& score) :
  Saucer(Configuration::Textures::BigSaucer,world, score)
{
  _world.add(Configuration::Sounds::SaucerSpawn1);
  _impulse *= 300.f;
}

//-----------------------------------------------------------------------
// getPoints
//-----------------------------------------------------------------------
int BigSaucer::getPoints()const
{
  return 50;
}

//-----------------------------------------------------------------------
// SmallSaucer constructor
//-----------------------------------------------------------------------
SmallSaucer::SmallSaucer(World &world, Score& score):
  Saucer(Configuration::Textures::SmallSaucer, world, score),
  _timeSinceLastShoot(sf::Time::Zero)
{
  _world.add(Configuration::Sounds::SaucerSpawn2);
  _impulse *= 400.f;
}

//-----------------------------------------------------------------------
// getPoints
//-----------------------------------------------------------------------
int SmallSaucer::getPoints() const
{
  return 200;
}

//-----------------------------------------------------------------------
// update
//-----------------------------------------------------------------------
void SmallSaucer::update(sf::Time deltaTime)
{
  Saucer::update(deltaTime);
  _timeSinceLastShoot += deltaTime;
  if (_timeSinceLastShoot > sf::seconds(1.5)) {
    if (_world.getPlayer()) {
      _world.add(std::unique_ptr<Entity>(new ShootSaucer(*this, getScore())));
      _timeSinceLastShoot = sf::Time::Zero;
    }
  }
}
