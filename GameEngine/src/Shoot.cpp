#include "Shoot.hpp"
#define _USE_MATH_DEFINES
#include <cmath>
#include "Random.hpp"

//-----------------------------------------------------------------------
// Shoot constructor
//-----------------------------------------------------------------------
Shoot::Shoot(Configuration::Textures id, World& world, Score& score):
  Entity(id, world, score)
{
  float angle = Tools::Random<float>(0.f, 2.f*M_PI);
  _impulse = sf::Vector2f(std::cos(angle), std::sin(angle));
}

//-----------------------------------------------------------------------
// update
//-----------------------------------------------------------------------
void Shoot::update(sf::Time deltaTime)
{
  float seconds = deltaTime.asSeconds();
  _sprite.move(seconds * _impulse);
  _duration -= deltaTime;
  if (_duration < sf::Time::Zero) {
    _alive = false;
  }
}
