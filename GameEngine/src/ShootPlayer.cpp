#include "ShootPlayer.hpp"
#include "Player.hpp"
#define _USE_MATH_DEFINES
#include <cmath>
#include "Enemy.hpp"
#include "Collision.hpp"

//-----------------------------------------------------------------------
// ShootPlayer constructor
//-----------------------------------------------------------------------
ShootPlayer::ShootPlayer(Player& from, Score& score):
  Shoot(Configuration::Textures::ShootPlayer, from.getWorld(), score)
{
  _duration = sf::seconds(5);
  float angle = from.getSprite().getRotation() / 180 * M_PI - M_PI/2;
  _impulse = sf::Vector2f(std::cos(angle), std::sin(angle)) * 500.f;
  setPosition(from.getPosition());
  _sprite.setRotation(from.getSprite().getRotation());
  _world.add(Configuration::Sounds::LaserPlayer);
}

//-----------------------------------------------------------------------
// isCollide
//-----------------------------------------------------------------------
bool ShootPlayer::isCollide(const Entity& other)const
{        
  if(dynamic_cast<const Enemy*>(&other) != nullptr) {
    return Collision::circleTest(_sprite,other.getSprite());
  }
  return false;
}
