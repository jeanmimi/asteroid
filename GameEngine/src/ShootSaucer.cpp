#include "ShootSaucer.hpp"
#include "Saucer.hpp"
#define _USE_MATH_DEFINES
#include <cmath>
#include "Random.hpp"
#include "World.hpp"
#include "Player.hpp"
#include "Meteor.hpp"
#include "Collision.hpp"
#include "Score.hpp"

//-----------------------------------------------------------------------
// ShootSaucer constructor
//-----------------------------------------------------------------------
ShootSaucer::ShootSaucer(SmallSaucer& from, Score& score):
  Shoot(Configuration::Textures::ShootSaucer, from.getWorld(), score)
{
  _duration = sf::seconds(5);
  sf::Vector2f vSaucerPlayer = _world.getPlayer()->getPosition() - from.getPosition();
  float accuracy_lost = Tools::Random<float>(-1.f,1.f)*M_PI/((200+_score.getScore())/100.f);
  float angle_rad = std::atan2(vSaucerPlayer.y,vSaucerPlayer.x) + accuracy_lost;
  float angle_deg = angle_rad * 180.f / M_PI;
  _impulse = sf::Vector2f(std::cos(angle_rad), std::sin(angle_rad)) * 500.f;
  // the shoot start from the current saucer position
  setPosition(from.getPosition());
  _sprite.setRotation(angle_deg + 90);
  _world.add(Configuration::Sounds::LaserEnemy);
}

//-----------------------------------------------------------------------
// isCollide
//-----------------------------------------------------------------------
bool ShootSaucer::isCollide(const Entity& other)const
{
  if(dynamic_cast<const Player*>(&other) || dynamic_cast<const Meteor*>(&other)) {
    return Collision::circleTest(_sprite,other.getSprite());
  }
  return false;
}
