#include "World.hpp"
#include "Entity.hpp"
#include "Configuration.hpp"
#include "Player.hpp"
#include "Enemy.hpp"
#include <algorithm>
#include <iostream>

//-----------------------------------------------------------------------
// World constructor
//-----------------------------------------------------------------------
World::World(float x, float y):
  _x(x),_y(y)
{}

//-----------------------------------------------------------------------
// World destructor
//-----------------------------------------------------------------------
World::~World()
{
  clear();
}

//-----------------------------------------------------------------------
// add
//-----------------------------------------------------------------------
void World::add(std::unique_ptr<Entity> entity)
{
  _entitiesTmp.emplace_back(std::move(entity));
}

//-----------------------------------------------------------------------
// clear
//-----------------------------------------------------------------------
void World::clear()
{
  _entities.clear();
  _entitiesTmp.clear();
  _sounds.clear();
}

//-----------------------------------------------------------------------
// add(Configuration::Sounds)
//-----------------------------------------------------------------------
void World::add(Configuration::Sounds id)
{
  std::unique_ptr<sf::Sound> sound(new sf::Sound(Configuration::soundManager.get(id)));
  sound->setAttenuation(0);
  sound->play();
  _sounds.emplace_back(std::move(sound));
}

//-----------------------------------------------------------------------
// isCollide
//-----------------------------------------------------------------------
bool World::isCollide(const Entity& other)
{
  return std::any_of(_entities.begin(), _entities.end(), [&other](std::unique_ptr<Entity>& entity){ return other.isCollide(*entity);});
}

//-----------------------------------------------------------------------
// size
//-----------------------------------------------------------------------
int World::size()
{
  return _entities.size() + _entitiesTmp.size();
}

//-----------------------------------------------------------------------
// getX
//-----------------------------------------------------------------------
int World::getX() const
{
  return _x;
}

//-----------------------------------------------------------------------
// getY
//-----------------------------------------------------------------------
int World::getY() const
{
  return _y;
}

//-----------------------------------------------------------------------
// getEntities
//-----------------------------------------------------------------------
const std::list<std::unique_ptr<Entity>>& World::getEntities() const
{
  return _entities;
}

//-----------------------------------------------------------------------
// getPlayer
//-----------------------------------------------------------------------
Player* World::getPlayer()
{
  Player *player = nullptr;
  auto res = std::find_if(_entities.begin(), _entities.end(),[](std::unique_ptr<Entity>& entity){return dynamic_cast<Player *>(entity.get()) != nullptr;});
  if (res != _entities.end()) {
    player = dynamic_cast<Player *>(res->get());
  } 
  
  return player;
}

//-----------------------------------------------------------------------
// clampPosition
//-----------------------------------------------------------------------
void World::clampPosition(Entity& entity)
{
  sf::Vector2f pos = entity.getPosition();

  if(pos.x < 0)  {
    pos.x = _x;
    pos.y = _y - pos.y;
  } else if (pos.x > _x) {
    pos.x = 0;
    pos.y = _y - pos.y;
  }

  if(pos.y < 0) {
    pos.y = _y;
  } else if(pos.y > _y) {
    pos.y = 0;
  }

  entity.setPosition(pos);

}

//-----------------------------------------------------------------------
// update
//-----------------------------------------------------------------------
void World::update(sf::Time deltaTime)
{
  if(_entitiesTmp.size() > 0) {
    _entities.splice(_entities.end(),_entitiesTmp);
  }

  //moving loop
  for(auto& entity : _entities) {
    entity->update(deltaTime);
    clampPosition(*entity);
  }

  //collision loop
  const auto end = _entities.end();
  for(auto it_i = _entities.begin(); it_i != end; ++it_i) {
    Entity& entity_i = **it_i;
    auto it_j = it_i;
    it_j++;
    for(; it_j != end;++it_j) {
      Entity& entity_j = **it_j;
      
      if(entity_i.isAlive() && entity_i.isCollide(entity_j)) {
	      entity_i.onDestroy();
      }
      
      if(entity_j.isAlive() && entity_j.isCollide(entity_i)) {
	      entity_j.onDestroy();
      }
    }
  }

  //delete dead entities and remove from world
  for(auto it = _entities.begin(); it != _entities.end();)  {
    if(!(*it)->isAlive()) {
      it = _entities.erase(it);
    } else {
      ++it;
    }
  }
  
  _sounds.remove_if([](const std::unique_ptr<sf::Sound>& sound) -> bool {
      return sound->getStatus() != sf::SoundSource::Status::Playing;
    });

}

//-----------------------------------------------------------------------
// draw
//-----------------------------------------------------------------------
void World::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
  for (auto& entity: _entities) {
    target.draw(*entity, states);
  }
}
