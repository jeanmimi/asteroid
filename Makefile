ifeq ("$(OS)","Windows_NT")
  SFMLINC = $(SFML_INSTALL)/include
  SFMLLIB = $(SFML_INSTALL)/bin
  LDPATH = -L../ -L$(SFMLLIB)
  SFMLGRAPHICS = sfml-graphics-2
  SFMLWINDOW = sfml-window-2
  SFMLSYSTEM = sfml-system-2
  SFMLAUDIO = sfml-audio-2
  OPENGL = openal32
  PREFIX =
  DLL = dll
  EXE = exe
  PIC =
else
  SFMLINC = /usr/include
  LDPATH = -L../
  SFMLGRAPHICS = sfml-graphics
  SFMLWINDOW = sfml-window
  SFMLSYSTEM = sfml-system
  SFMLAUDIO = sfml-audio
  OPENGL = GL
  PREFIX = lib
  DLL = so
  EXE = out
  PIC = -fPIC
endif

GLOBALINC = -I./include -I$(SFMLINC)

export
LIBS = GameEngine Events Resource
SUBDIRS = $(LIBS) Asteroid

.PHONY: subdirs $(SUBDIRS)

subdirs: $(SUBDIRS)

$(SUBDIRS):
	$(MAKE) -C $@

GameEngine: Events Resource
Asteroid: $(LIBS)

clean:
	for d in $(SUBDIRS); \
	do                               \
	  $(MAKE) --directory=$$d clean; \
	done
	rm *.$(DLL)


