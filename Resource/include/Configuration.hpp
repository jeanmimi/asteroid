#ifndef _CONFIGURATION_HPP
#define _CONFIGURATION_HPP

#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include "ResourceManager.hpp"
#include "ActionMap.hpp"


class Configuration
{
public:
  //this is a singleton
  Configuration() = delete;
  Configuration(const Configuration&) = delete;
  Configuration& operator=(const Configuration&) = delete;

  enum Textures : int {
    Background,
    Player,
    PlayerLife,
    BigSaucer,
    SmallSaucer,
    BigMeteor1,
    BigMeteor2,
    BigMeteor3,
    BigMeteor4,
    MediumMeteor1,
    MediumMeteor2,
    SmallMeteor1,
    SmallMeteor2,
    SmallMeteor3,
    SmallMeteor4,
    ShootPlayer,
    ShootSaucer
  };
  static ResourceManager<sf::Texture, int> textureManager;

  enum Fonts : int {Gui};
  static ResourceManager<sf::Font,int> fontManager;

  enum PlayerInputs: int {Up, Left, Right, Shoot, Hyperspace};
  static ActionMap<int> playerInputs;

  enum Sounds : int {
                LaserPlayer,
                LaserEnemy,
                SaucerSpawn1,
                SaucerSpawn2,
                Boom,
                Boom2,
                Explosion1,
                Explosion2,
                Explosion3,
                Jump
            };
  static ResourceManager<sf::SoundBuffer,int> soundManager;
  
  static void initialize();
  
private:
  static void initTextures();
  static void initSounds();
  static void initFonts();
  static void initPlayerInputs();

};
  
#endif
