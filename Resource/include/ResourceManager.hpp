#ifndef _RESOURCEMANAGER_HPP
#define _RESOURCEMANAGER_HPP

#include <unordered_map>
#include <memory>
#include <SFML/Audio/Music.hpp>

template<typename RESOURCE, typename IDENTIFIER = int>
class ResourceManager
{
public:
  ResourceManager() = default;
  ~ResourceManager() = default;
  ResourceManager(const ResourceManager&) = delete;
  ResourceManager& operator=(const ResourceManager&) = delete;

  template<typename ... Args>
  void load(const IDENTIFIER& id, Args&& ... args);

  RESOURCE& get(const IDENTIFIER& id) const;

private:
  std::unordered_map<IDENTIFIER, std::unique_ptr<RESOURCE>> _hmap;
};

template<typename IDENTIFIER>
class ResourceManager<sf::Music, IDENTIFIER>
{
public:
  ResourceManager() = default;
  ~ResourceManager() = default;
  ResourceManager(const ResourceManager&) = delete;
  ResourceManager& operator=(const ResourceManager&) = delete;

  template<typename ... Args>
  void load(const IDENTIFIER& id, Args&& ... args);

  sf::Music& get(const IDENTIFIER& id) const;

private:
std::unordered_map<IDENTIFIER, std::unique_ptr<sf::Music>> _hmap;
};

#include "ResourceManager_impl.hpp"
#endif
