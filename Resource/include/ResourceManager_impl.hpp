template<typename RESOURCE, typename IDENTIFIER>
template<typename ... Args>
void ResourceManager<RESOURCE,IDENTIFIER>::load(const IDENTIFIER& id, Args&& ... args)
{
  std::unique_ptr<RESOURCE> ptr(new RESOURCE);
  if (!ptr->loadFromFile(std::forward<Args>(args)...)) {
    throw std::runtime_error("impossible to load file");
  }
  _hmap.emplace(id, std::move(ptr));
}

template<typename IDENTIFIER>
template<typename ... Args>
void ResourceManager<sf::Music,IDENTIFIER>::load(const IDENTIFIER& id, Args&& ... args)
{
  std::unique_ptr<sf::Music> ptr(new sf::Music);
  if (!ptr->openFromFile(std::forward<Args>(args)...)) {
    throw std::runtime_error("impossible to load file");
  }
  _hmap.emplace(id, std::move(ptr));
}

template<typename RESOURCE, typename IDENTIFIER>
RESOURCE& ResourceManager<RESOURCE,IDENTIFIER>::get(const IDENTIFIER& id) const
{
  return *_hmap.at(id);
}

template<typename IDENTIFIER>
sf::Music& ResourceManager<sf::Music,IDENTIFIER>::get(const IDENTIFIER& id) const
{
  return *_hmap.at(id);
}
