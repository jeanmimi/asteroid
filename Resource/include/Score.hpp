#ifndef _SCORE_HPP
#define _SCORE_HPP

#include "SFML/Graphics.hpp"
#include <vector>

class Score: public sf::Drawable
{
  public:
    Score();
    Score(const Score &) = delete;
    const Score &operator=(const Score &) = delete;
    void reset();
    void addScore(int s);
    int getScore() const;
    int getLevel() const;
    void increaseLevel();
    void loseLife();
    bool isGameOver() const;
    virtual void draw(sf::RenderTarget &target, sf::RenderStates states) const override;

  private:
    int _level;
    int _lives;
    int _score;
    sf::Text _txt_score;
    sf::Sprite _background;
    std::vector<sf::Sprite> _spritesLife;

};

#endif