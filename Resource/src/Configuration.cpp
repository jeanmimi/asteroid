#include "Configuration.hpp"
#include "Random.hpp"

ResourceManager<sf::Texture, int> Configuration::textureManager;
ResourceManager<sf::Font,int> Configuration::fontManager;
ResourceManager<sf::SoundBuffer, int> Configuration::soundManager;
ActionMap<int> Configuration::playerInputs;


void Configuration::initialize()
{
  initTextures();
  initFonts();
  initSounds();
  initPlayerInputs();
  Tools::InitRandom();
 }

void Configuration::initTextures()
{
  //background
  textureManager.load(Textures::Background, "media/textures/milkyway.jpg");
  //player
  textureManager.load(Textures::Player,"media/textures/Ship.png");
  textureManager.load(Textures::PlayerLife,"media/textures/life.png");
  //saucers
  textureManager.load(Textures::BigSaucer,"media/textures/Big.png");
  textureManager.load(Textures::SmallSaucer,"media/textures/Small.png");
  //meteors
  textureManager.load(Textures::BigMeteor1,"media/textures/Big1.png");
  textureManager.load(Textures::BigMeteor2,"media/textures/Big2.png");
  textureManager.load(Textures::BigMeteor3,"media/textures/Big3.png");
  textureManager.load(Textures::BigMeteor4,"media/textures/Big4.png");
  
  textureManager.load(Textures::MediumMeteor1,"media/textures/Medium1.png");
  textureManager.load(Textures::MediumMeteor2,"media/textures/Medium2.png");
  
  textureManager.load(Textures::SmallMeteor1,"media/textures/Small1.png");
  textureManager.load(Textures::SmallMeteor2,"media/textures/Small2.png");
  textureManager.load(Textures::SmallMeteor3,"media/textures/Small3.png");
  textureManager.load(Textures::SmallMeteor4,"media/textures/Small4.png");
  //lasers
  textureManager.load(Textures::ShootPlayer,"media/textures/Player.png");
  textureManager.load(Textures::ShootSaucer,"media/textures/Saucer.png");
}

void Configuration::initFonts()
{
  fontManager.load(Fonts::Gui,"media/fonts/trs-million.ttf");
}

void Configuration::initSounds()
{
  
  //laser
  soundManager.load(Sounds::LaserPlayer,"media/sounds/laser1.ogg");
  soundManager.load(Sounds::LaserEnemy,"media/sounds/laser2.ogg");
  //saucers
  soundManager.load(Sounds::SaucerSpawn1,"media/sounds/spawn1.flac");
  soundManager.load(Sounds::SaucerSpawn2,"media/sounds/spawn2.flac");
  // Boom
  soundManager.load(Sounds::Boom,"media/sounds/boom.flac");
  soundManager.load(Sounds::Boom2,"media/sounds/boom2.flac");
  // Explosion
  soundManager.load(Sounds::Explosion1,"media/sounds/explosion1.flac");
  soundManager.load(Sounds::Explosion2,"media/sounds/explosion2.flac");
  soundManager.load(Sounds::Explosion3,"media/sounds/explosion3.flac");
  //others
  soundManager.load(Sounds::Jump,"media/sounds/hyperspace.flac");
  
}

void Configuration::initPlayerInputs()
{
  playerInputs.set(PlayerInputs::Up, Action(sf::Keyboard::Up));
  playerInputs.set(PlayerInputs::Right, Action(sf::Keyboard::Right));
  playerInputs.set(PlayerInputs::Left, Action(sf::Keyboard::Left));
  playerInputs.set(PlayerInputs::Hyperspace,Action(sf::Keyboard::Down,Action::Type::Released));
  playerInputs.set(PlayerInputs::Shoot,Action(sf::Keyboard::Space));
}
