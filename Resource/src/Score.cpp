#include "Score.hpp"
#include "Configuration.hpp"

Score::Score():
_level(-1), _lives(-1), _score(-1)
{
  sf::Font& font = Configuration::fontManager.get(Configuration::Fonts::Gui);
  _txt_score.setFont(font);
  _txt_score.setCharacterSize(24);

  sf::Texture& textureBg = Configuration::textureManager.get(Configuration::Textures::Background);
  _background.setTexture(textureBg);
}
void Score::reset()
{
  _level = 1;
  _lives = 3;
  _score = 0;
  _spritesLife.clear();
  for(int i = 0;i< _lives;++i) {
    sf::Texture& texturePlayer = Configuration::textureManager.get(Configuration::Textures::PlayerLife);
    sf::Sprite sprLife;
    sprLife.setTexture(texturePlayer);
    sprLife.setPosition(40*i,40);
    _spritesLife.push_back(sprLife);
    
  }
  
  _txt_score.setString("0 Level 1");
}

void Score::addScore(int s)
{
  int old_score = _score;
  _score += s*_level;
  _lives += _score/10000 - old_score/10000;
  int nbSprLives = _spritesLife.size();
  if (_lives > nbSprLives) {
    for(int i = nbSprLives; i < _lives;++i) {
      sf::Sprite sprLife(Configuration::textureManager.get(Configuration::Textures::PlayerLife));
      sprLife.setPosition(40*i,40);
      _spritesLife.push_back(sprLife);
    }
  }
  std::string sTxtScore = std::to_string(_score)+" Level "+std::to_string(_level);
  _txt_score.setString(sTxtScore);
}

int Score::getScore() const
{
  return _score;
}

int Score::getLevel() const
{
    return _level;
}

void Score::increaseLevel()
{
    ++_level;
}

void Score::loseLife()
{
  --_lives;
  if (!_spritesLife.empty()) {
    _spritesLife.pop_back();
  }
}

bool Score::isGameOver() const
{
  return _lives < 0;
}

void Score::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
  target.draw(_background);
  target.draw(_txt_score);
  for(auto& spr: _spritesLife) { 
    target.draw(spr);
  }
}
