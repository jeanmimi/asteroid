#ifndef _RANDOM_HPP
#define _RANDOM_HPP

#include <random>
#include <memory>
#include <chrono>

namespace Tools
{
  template<typename T>
  struct  Distribution {};

  template<>
  struct Distribution<int> {
    using type = std::uniform_int_distribution<int>;
  };

  template<>
  struct Distribution<float> {
    using type = std::uniform_real_distribution<float>;
  };

  template<>
  struct Distribution<double> {
    using type = std::uniform_real_distribution<double>;
  };

  using Generator = std::minstd_rand;
  static Generator _gen;
  inline void InitRandom()
  {
    unsigned int seed = std::chrono::system_clock::now().time_since_epoch().count();
    _gen.seed(seed);
  }

  
  template<typename T >
  class Random
  {
    // Types

    
  public:

    using DistributionType = typename Distribution<T>::type;

    
    Random( T minVal, T maxVal ):
      _dis(new DistributionType( minVal, maxVal))
    {}
 
    ~Random() = default;
 
    operator const T()
    {
      return _dis->operator()(_gen);
    }

  private:

    std::unique_ptr<DistributionType> _dis;

  };

  //template <typename T>
  //Generator Random<T>::_gen;
}

#endif
